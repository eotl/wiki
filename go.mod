module codeberg.org/eotl/wiki

go 1.19

require (
	codeberg.org/eotl/bootstrap-eotl v0.0.0-20231108072808-054ee7d83d18 // indirect
	codeberg.org/eotl/hugo-mods/debug v0.0.0-20240322163312-749925ccbca6 // indirect
	codeberg.org/eotl/hugo-mods/edited v0.0.0-20240316124047-1678d427f5f4 // indirect
	codeberg.org/eotl/hugo-mods/recently v0.0.0-20240316124047-1678d427f5f4 // indirect
	codeberg.org/eotl/hugo-mods/wiki v0.0.0-20240322163312-749925ccbca6 // indirect
	github.com/hugomods/icons v0.6.4 // indirect
	github.com/twbs/bootstrap v5.3.2+incompatible // indirect
	github.com/twbs/icons v1.11.3 // indirect
)
