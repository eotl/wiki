---
title: "Aero & Hydroponics"
draft: false
leafs: ["aero", "hydroponic"]
date: "2020-11-04 11:51:51"
date_created: "2020-11-04 11:51:51"
type: "wiki"
branches: "essentials"
---

- [Self-sustaining farm for Wisconsin cold via backyard aeroponics](https://youtube.com/watch?v=H4gsnFJRAB0) (video)
- [Art Garden](https://www.youtube.com/c/ArtGardenLLC/videos) (YouTube channel)
- [Art Garden Brings Families Together Over Food](https://www.powerhousehydroponics.com/new-product-art-garden-brings-families-together-over-food/)

### Companies

- [Art Garden Growing Systems](https://artgardengrowingsystems.com)
