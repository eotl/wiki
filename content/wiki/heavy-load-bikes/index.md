---
title: "Heavy Load Bikes"
date: "2020-11-14 12:19:00"
date_created: "2020-11-14 12:19:00"
type: "wiki"
leafs: ["bicycles", "cargo", "logistics"]
branches: "transportation"
---

The following is a proposal for a collectively owned (or just shared) "heavy
load" cargo bicycle. These are not the usual box style cargo bike people use to
transport kids and groceries, but the kind you can transport 20 boxes of
beverages with. The immediate uses cases for this bike would be:

### Urban Arrow Tender

![Tender 1500 Post & Parcel.jpg](Tender%201500%20Post%20%26%20Parcel.jpg)

* [Website](https://www.urbanarrow.com/en/tender)
* Cost: €14,000
* Capacity: 300k load capacity
* Style: mostly motor assist, but can still be pedaled if batteries are dead

---

### Icai

![Icai.png](Icai.png)

* [Website](https://icai.de)
* Cost: €12,000
* Capacity: 300kg load capacity..
* Style: relies 100% on motor
