---
title: "Shipping Palettes"
date: "2021-01-14 12:19:00"
date_created: "2021-01-14 12:19:00"
type: "wiki"
leafs: ["shipping", "logistitcs"]
branches: "containers"
---

## Hacks

Many crafty people have figured out uses repurposing wooden shipping palettes.

- [uH Bench](uH-Bench.pdf)
