---
title: "Composting, Fertilizers, Worms"
draft: false
leafs: ["composting", "fertilizers", "worm"]
date: "2022-06-23 18:00:08"
date_created: "2022-06-23 18:00:08"
type: "wiki"
branches: "essentials"
---

Growing things requires soil to be rich with nutrients. The following
sections are techniques to produce nutrients sustainably.


### Composting

- [Leaf mold compost shows benefit for tomato plants in degraded urban soils](https://www.agronomy.org/news/science-news/leaf-mold-compost-shows-benefit-tomato-plants-degraded-urban-soils/)


### Fertilizers

- [Sanitized human urine (Oga) as a fertilizer auto-innovation from women farmers in Niger](https://link.springer.com/article/10.1007/s13593-021-00675-2)

### Worms

Ready made worm boxes

- [Wurmkiste](https://wurmkiste.at) €79 - 179
- [Terrabox](https://www.terrabox.bio) €250
- [WurmUp](https://eu.wormup.ch/#!/WormUp-HOME-Wurmkomposter-aus-Keramik/p/101071244/category=0) €279
