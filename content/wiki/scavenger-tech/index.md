---
title: "Scavenger Tech"
draft: false
leafs: ["scavenger", "tech"]
date: "2022-03-17 11:04:54"
date_created: "2022-03-17 11:04:54"
type: "wiki"
branches: "network"
---

Inspired by the approach of [CollapseOS](https://collapseos.org) goal of
"scavenging" parts to build new repurposed systems for specific or
general purpose computing, but branches from the concept of
[permacomputing](/permacomputing) in that scavenger tech creates new end
tools out of old broken or neglected parts. While permacomputing aims
for creating new, but longer term, more sustainable systems.

**Examples**

- [Mini Computer Easel](Mini Computer Easel)

Maybe they are one in the same and this is just semantics, but maybe
they are also divergent approaches and worth documenting separately.
