---
title: "Urban Gardening & Farming"
draft: false
leafs: ["urban", "gardening"]
date: "2020-12-13 23:08:30"
date_created: "2020-12-13 23:08:30"
type: "wiki"
branches: "essentials"
---


Effective gardening in urban environments can be challenging. Space,
light, and soil in cities is usually all quite different from
traditional rural farming. Here is a page to collect info tips about
novel approaches.

### Why Urban Gardening?

As of a Feb, 2023 study from UT Austin showing [urban gardens are good for ecosystems and humans](https://news.utexas.edu/2023/02/07/urban-gardens-are-good-for-ecosystems-and-humans/)



#### Indoor Apartment Farming

![Terrace Permaculture
Gardening](/terrace_permaculture_gardening.pdf){.align-center}

An easy to understand overview of doing a permaculture gardening on apartment
balconies. ![Original
source](https://the-eye.eu/public/Site-Dumps/library.uniteddiversity.coop/Permaculture/Terrace_Permaculture_Gardening.pdf)

#### Bio Scrap Composting

Buying fertilizer to grow plants costs money and usually comes packed in
plastic. Meanwhile, all the scraps of vegetables and fruit that you
normally put in the trash can be used to make your own fertilizer. But
it\'s not as easy as just that, here we can collect information and tips
about composting your own fertilizer.

#### Watering Systems

Watering plants takes time and requires one to be available. In urban
settings people are often busy and traveling. Luckily, there are
numerous ways to keep plants hydrated for busy people on the go and
solutions range from simple DIY string to fancy open-source robots.

- [DIY Self-Watering System for Houseplants](https://scissorsandsage.com/2015/04/20/diy-self-watering-system-for-houseplants/)
- [Ollas - der Wasserspeicher für das Beet](https://www.zerowastefamilie.de/Ollas-_-der-Wasserspeicher-f.ue.r-das-Beet.htm) (DE)
- [Ollas selber bauen: Bewässerung mit natürlicher Tropfschlauch-Alternative](https://www.smarticular.net/ollas-selber-bauen-bewaesserungssystem-garten-hochbeet/) (DE)
- [FarmBot](https://farm.bot) - an open-source robotic farming system
- [BLUMAT Wasserspender für Zimmerpflanzen](https://www.amazon.de/dp/B000LLOUHW/ref=cm_sw_r_cp_api_i_1gUMEbX6W0P24) - (Amazon DE)


### Significant Projects

**Ron Finley the Gangsta Gardener**

- [A guerilla gardener in South Central LA \| Ron Finley](https://www.ted.com/talks/ron_finley_a_guerrilla_gardener_in_south_central_la) (TED)
- [For MasterClass' Ron Finley, growing a garden is a revolution](https://www.latimes.com/lifestyle/story/2020-07-10/gardening-with-gangsta-gardener-and-masterclass-teacher-ron-finley) (Los Angeles Times)
- [Ron Finley: Urban Gangsta Gardener in South Central LA \| Game Changers](https://www.youtube.com/watch?v=7t-NbF77ceM) (YouTube)

**Michigan Urban Farming Initiative (MUFI)**

MUFI is a pretty significant seeming sustainable urban agriculture
non-profit in Detroit created from an abandoned apartment building that
sprawls over a few blocks.

- [Michigan Urban Farming Initiative](https://www.miufi.org) official website
- [America's first sustainable urban agrihood](https://www.youtube.com/watch?v=9ZeKXInnt1U) (YouTube video)

**The Urban Farming Guys**

- [Urban Farming Guys](https://theurbanfarmingguys.com) - Official website
- [Farmin\' in The Hood 2](https://www.youtube.com/watch?v=T-vyjyK6_kg) (YouTube video)

**Dervaes Family Farm**

A 380 sq meter (4000 sq foot) house and yard based in Pasadena, CA that
was converted to a significant urban farming operation.

- [The Urband Homestead](https://urbanhomestead.org) - Official website
- [Urban Farms on KCET](https://www.youtube.com/watch?v=qJAnBGHhjAc) - YouTube video
- [Dervaes Family Farm - The Urban Homestead - Episode 94](https://www.youtube.com/watch?v=v6Dnbem1rVE) - YouTube video
