---
title: "Bicycle Trailers"
draft: false
leafs: ["bicycle", "trailer"]
date: "2022-05-21 06:56:40"
date_created: "2022-05-21 06:56:40"
type: "wiki"
branches: "transportation"
---

Need to transport more than a standard cargo bike? A trailer is a good
option:

- [Cargo Carla](https://www.carlacargo.de)
- [Hinterher](https://www.hinterher.com)
