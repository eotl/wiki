---
title: "Bulk Storage Containers"
draft: false
leafs: ["professional", "food", "container"]
date: "2022-03-18 15:42:41"
date_created: "2022-03-18 15:42:41"
type: "wiki"
branches: "food-storage"
---

To figure out how many and what size of containers you need, you need to
use kilogram to liter
[converter](https://rechneronline.de/kueche/gewicht.php) to figure out
that for say 25 kg of rice you need about 28 liters of storage.

The following are a list of German companies to source long term storage
containers from.

**Long Term Storage (Plastic)**

- [BRB Lagertechnik](https://www.brb-lagertechnik.de/behaelter-und-kaesten/lebensmittelbehaelter/gn-behaelter/gn-vorratsbehaelter) has large high quality plastic containers certified food grade of affordable prices (up to 28L).
- [Nisbets](https://www.nisbets.de/kuechenbedarf-und-lagerung/lebensmittellagerung/einmachglaeser/_/a33-3) has nice attractive high quality glass jars (1 - 3L range, one 6.2L).
- [Interfastro plastic](https://www.intergastro.de/baeckerei/behaelter-wagen/zutatenbehaelter/) is a range of higher quality industrial food grade storage containers.
- [Eimer und Becher Profi](https://www.eimerundbecherprofi.de/eimer/eckige-eimer/) have super affordable offerings, but the fine print seemingly says "one-time" use containers, which seems to mean not food grade reusable.

**Long Term Storage (Glass)**

- [Intergastro glass](https://www.intergastro.de/kueche/behaelter-schuesseln/einmachglaeser/) has quite good glass canning jars and such. Brands like Weck, Quattro Stagioni.
- [Kilner 4L](https://www.nisbets.de/kilner-vorratsglas-4l/cn619)
- [Hawos Glass silos](https://hawos.de/en/product/hawos-glass-bulk-bin-silo/)

**Cloth Bags**

![](/hawos-grain-saecke.jpg){width="400"}

- [Hawos grain sacks](https://hawos.de/en/product/hawos-grain-sacks/) - hanging sacks with an opening at bottom

### Traditional Methods

**kyōgi (経木)** - a traditional Japanese wooden wrapping paper

- [@WrathOfGnon](https://twitter.com/wrathofgnon/status/1618487378133544960)
