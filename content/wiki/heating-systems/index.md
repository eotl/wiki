---
title: "Heating Systems"
draft: false
date: "2020-05-04 19:07:30"
date_created: "2020-05-04 19:07:30"
type: "wiki"
leafs: ["heating", "system"]
branches: "machines-appliances"
---

For colder climates heating is essential. How to go about this in a way
that sustainable is difficult. In big cities with modern buildings,
centralized heating systems are hard to beat. For more rural and
decentralized small living setups, the challenge exists. This is a page
to document ideas in this direction.

### Rocket Stoves

A [rocket stove](https://en.wikipedia.org/wiki/Rocket_stove) is a
specific kind of stove relies on small sized wood fuel. Due to the
design rocket stoves serve a duel purpose of both heating and cooking.

#### Create Your Own

- [A Rocket Stove Made From a Five Gallon Metal
    Bucket](https://www.rootsimple.com/2012/03/a-rocket-stove-made-from-a-five-gallon-metal-bucket/)

#### Manufactured Rocket Stoves

- [ACTIVA Eintopfofen, Ungarischer Ofen Gulaschkessel Gulaschkanone Grill Barbeque Eintopf Ofen Gulasch Kessel, Gulaschofen](https://archive.is/6Wy6t) - €109,99
- [BBQ-Toro Raketenofen Rakete \#2](https://archive.is/kkUYL) - €124,95
- [BBQ-Toro Raketenofen Rakete \#4 I Edelstahl Rocket Stove inkl. 20 L Kochtopf](https://archive.is/Gnh0X) - €249,95
- [Kaminofen mit Backfach und Herdplatte 93891Kamin Ofen Werkstattofen 8,4 kW Schwedenofen](https://archive.is/csBei) - €449,00
