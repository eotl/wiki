---
title: "Refrigeration"
draft: false
leafs: ["refrigeration"]
date: "2022-06-07 12:14:46"
date_created: "2022-06-07 12:14:46"
type: "wiki"
branches: "network"
---

Keeping foods fresh as long as possible.

### Water Submersion

There is a brilliantly simple archaic method of preserving fruit and
vegetables as [highlighted by](https://twitter.com/wrathofgnon/status/1317006542164553728) the
amazing <https://twitter.com/WrathOfGnon> Twitter account which is
simply putting fruit & veggies in water.

![](/wofg-water-veggie-refrigeration-1.jpeg){width="400"}

![](/wofg-water-veggie-refrigeration-2.jpeg){width="400"}

![](/wofg-water-veggie-refrigeration-3.jpeg){width="400"}

![](/wofg-water-veggie-refrigeration-4.jpeg){width="400"}
