---
title: "Interesting Cooperatives"
draft: false
leafs: ["interesting", "cooperative"]
date: "2022-02-28 21:03:57"
date_created: "2022-02-28 21:03:57"
type: "wiki"
branches: "network"
---

The following is a non-exhaustive list of coops doing interesting things
that people don\'t normally think of when they think of coops.

### Spain

-   La Zona - aims to be an Amazon in Catalonia [](https://lazona.coop)

### France

-   Label Emmaus - [](https://www.label-emmaus.co/fr)
-   Mobicoop - a mobile phone data provider run as a coop [](https://www.mobicoop.fr)
-   TeleCoop - a mobile phone provider coop [](https://telecoop.fr)
-   Commown - a responsible electronics coop [](https://commown.coop)
-   RailCoop - a railway train cooperative society [](https://en.wikipedia.org/wiki/Railcoop)
-   CoopCircuits - an open food platform [](https://www.coopcircuits.fr)
-   FinaCoop - a financial services coop [](https://www.finacoop.fr)
