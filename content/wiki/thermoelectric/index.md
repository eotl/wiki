---
title: "Thermoelectric"
draft: false
leafs: ["thermoelectric", "energy"]
date: "2020-08-26 12:50:34"
date_created: "2020-08-26 12:50:34"
type: "wiki"
branches: "energy-production"
---

Thermoelectric is an interesting form of energy generation and capture
of runoff energy. Read about it in this article.

- [LOW←TECH MAGAZINE - Thermoelectric Stoves: Ditch the Solar Panels?](https://solar.lowtechmagazine.com/2020/05/thermoelectric-stoves-ditch-the-solar-panels.html)

### Manufacturers

- [Hi-Z](https://hi-z.com) - seems to have a good range of solid products
- [Thermomanic](http://www.thermonamic.com/) - Chinese company also have an [Alibaba shop](https://thermonamic.en.alibaba.com/)
- [Thermalforce](http://www.thermalforce.de) - website not working (Aug 2020)
- [Tellurex](https://tellurex.com) - out of business :/

### Products

- [tPOD1](https://www.kickstarter.com/projects/tpod1/tpod1-thermoelectric-power-on-demand/posts) - Kickstarter campaign for a small thermoelectric charger unit

### Resources

- [Custom Thermoelectric](https://customthermoelectric.com) - Lists various Thermoelectric manufactures products for Cooling, Heating, and Power Generation based on solid state semiconductor materials.
