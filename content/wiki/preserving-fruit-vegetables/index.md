---
title: "Preserving Fruit & Vegetables"
draft: false
leafs: ["preserving", "fruit", "vegetable"]
date: "2022-06-06 09:19:56"
date_created: "2022-06-06 09:19:56"
type: "wiki"
branches: "food-hacks"
---

Preserving Fruit & Vegetables
=============================

- [Refrigeration](/refrigeration)
- [Canning](/canning)
- [Vacuum Sealing](/vacuum-sealing)
- [Dehydrating](/dehydrating)
