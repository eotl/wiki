---
title: "Exchange Points"
draft: false
leafs: ["exchanges"]
date: "2022-06-06 09:38:19"
date_created: "2022-06-06 09:38:19"
type: "wiki"
branches: "essentials"
---

A very simple, often ignored (due it being incompatible with a
capitalist economy) approach to living more sustainably is simply
exchanging things you don\'t need for things you do. This is different
than barter trade whereby the exchange of goods is supposed to be equal.
The method of non-value based exchange is simpler and more flexible, but
also requires some form of community or connection.

Examples of locations:

- [Trial & Error: Taushladen](https://www.trial-error.org/projektraum/tauschladen/)
