---
title: "Tiny Homes"
draft: false
leafs: ["tiny-homes"]
date: "2022-03-01 17:10:01"
date_created: "2022-03-01 17:10:01"
type: "wiki"
branches: "housing"
---


The Tiny Home movement is a wonderful way of living more sustainably- if
you only have space that you absolutely need- you will use much less
energy. The web is full of amazing tiny homes, many of those are one-off
custom constructions that people create for themselves. This page is a
collection of companies that offer ready made Tiny Home kits or
completed builds.

Shipping containers

- http://www.tincancabin.com/2013/12/the-shipping-container-cabin-in-perspective/

Really swanky designer cabin

- https://www.treehugger.com/green-architecture/true-studio-modern-dwellings.html

Cool collection mall of containers

- https://www.treehugger.com/modular-design/stackt-instant-shipping-container-shopping-and-entertainment-spot-built-toronto.html


**Europe**

- [Iglucraft](https://iglucraft.com/en/) adorable wood shingled (and nicely finished interior) cabins, saunas, and workspaces starting at 19,900 EUR, made in Estonia.
- [SISI Containers](http://ageoftribes.net/sisu/) professional conversion of shipping containers into classy designed tiny homes, made in Germany.
- [Wohnwagon](https://www.wohnwagon.at/der-wohnwagon/) well crafted stylish tiny homes in a range of sizes and build levels, made in Austria

**Projects**

- [Hexayurt](http://hexayurt.com) zero-waste designs help you turn common building materials into cheap energy-efficient housing using few tools and simple skills
