---
title: "Radical Networks"
draft: false
leafs: ["radical", "networks"]
date: "2022-04-04 20:08:42"
date_created: "2022-04-04 20:08:42"
type: "wiki"
branches: "network"
---

- [Unsigned messaging](https://unsigned.io/private-messaging-over-lora/)
- [Reticulum](https://markqvist.github.io/Reticulum/manual/)
- [Decentralized Object Location and Routing: A New Networking Paradigm](https://sites.cs.ucsb.edu/~ravenben/publications/dissertation/)
