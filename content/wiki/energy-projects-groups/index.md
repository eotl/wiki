---
title: "Energy Projects & Groups"
draft: false
date: "2020-08-19 12:19:25"
date_created: "2020-08-19 12:19:25"
type: "wiki"
leafs: ["sustainable", "energy", "projects", "groups"]
branches: "energy-production"
---


- [Libre Solar](https://libre.solar) - Open source MPPT/PWM charge controllers (source code: [GitHub](https://github.com/LibreSolar))
- [Aleea](https://aleea.org) - Open source power converter for both solar and wind power (site in French)
- [SolarBlitz](https://twitter.com/MrChrisEllis/status/1289542969884905473) - A solar powered setup of the open source [RaspiBlitz](https://raspiblitz.org) Bitcoin/Lighting node

------------------------------------------------------------------------

- [Solar Punk Manifesto](Solar Punk Manifesto) - a crowd sourced rough draft
