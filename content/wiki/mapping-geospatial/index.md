---
title: "Mapping & Geospatial"
draft: false
leafs: ["mapping", "geospatia"]
date: "2022-02-26 15:26:53"
date_created: "2022-02-26 15:26:53"
type: "wiki"
branches: "information-technology"
---

**Geocoder APIs** are used to transform addresses into
latitude,longitude coordinates (geocoords). There are various webservice
APIs which do this.

- [Nomatim Openstreetmap](https://nominatim.openstreetmap.org/ui/search.html)
- [HERE REST APIs](https://developer.here.com/develop/rest-apis)
