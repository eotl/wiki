---
title: "Cooling Systems"
draft: false
leafs: ["cooling", "system"]
date: "2022-07-10 14:19:25"
date_created: "2022-07-10 14:19:25"
type: "wiki"
branches: "machines-appliances"
sections:
    - title: "Cooling Overview"
      content: "Lots to learn about **cooling**, which is the opposite of warming"
    - title: "Fans"
      content: "they wave blahb lah blah, more stuff"
    - title: "Freeze-rays"
      content: "not invented yet"
---

Throughout history, people living in different climates and terrains
have invented numerous methods of keeping their homes and bodies cool,
sustainably. It's only in the last century humans started creating
electricity intensive techniques.

Here we seek to document various methods which do not require
electricity or only a minimal amount.

### Air Conditioning

- [Evaporative cooler](https://en.wikipedia.org/wiki/Evaporative_cooler) also known as a swamp cooler
