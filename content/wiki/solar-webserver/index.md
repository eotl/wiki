---
title: "Solar Webservers"
draft: false
leafs: ["solar-power", "webserver"]
date: "2021-10-14 12:12:13"
date_created: "2021-10-14 12:12:13"
type: "wiki"
branches: "information-technology"
---

- [LOW←TECH MAGAZINE](https://solar.lowtechmagazine.com) - solar powered online magazine about radical sustainability
- [Solar Protocol](http://solarprotocol.net) - a network of solar powered servers that load balance

EOTL owns the domain name [eotl.solar](https://eotl.solar) and has
experimented with a scheme running a front-end proxy to allow solar
servers hosted at home behind a NAT to be accessible on the public
internet. This project is currently in hibernation, but just needing the
right interested party to revive it.
