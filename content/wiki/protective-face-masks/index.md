---
title: "Protective Face Masks"
draft: false
leafs: ["protective", "face", "mask"]
date: "2020-04-03 13:27:11"
date_created: "2020-04-03 13:27:11"
type: "wiki"
branches: "crisis-infrastructure"
---

Due to outbreak of Corona Virus (
[COVID-19](https://en.wikipedia.org/wiki/Coronavirus_disease_2019) )
which has been characterized as a [pandemic by World Health
Organization](https://twitter.com/WHO/status/1237777021742338049), there
is much demand for protective face masks. Over the counter supply in
many countries is non-existent. The specific effectiveness of masks in
relation to COVID-19 is full of conflicting information and actions.

### DIY Mask Pattern

This is a page to document such information as well as sustainable and
DIY protective face masks with filter pocket.

![](20200330_173221-01.jpeg)

![](01web.jpg) Cut two pieces each of lining and outer shell from prewashed fabrics.

![](02web.jpg) Sew along the middle seam, right sides together (pattern contains 1cm seam allowance).

![](03web.jpg) Clip little V\'s into the seam allowance making sure not to cut through the seam.

![](04web.jpg) Fold seam allowance to one side and stitch it down a few millimeters parallel to the existing seam.

![](05web.jpg) Iron the seam and cut away exess seam allowance.

![](06web.jpg) On the outer shell narrowly fold over the short edges and iron them flat.

![](07web.jpg) On the lining fold over short edges 1cm and iron them flat.

![](08web.jpg) Stitch down edges with zigzag stitch encasing the edge of the folded fabric.

![](09web.jpg) Pin shell and lining, right sides together and sew along the long edges with 1cm seam allowance.

![](010web.jpg) Clip V\'s into seam allowance and trim down to 0,7mm.

![](011aweb.jpg) Turn inside out and iron seams flat.

![](012web.jpg) Fold over short edge of shell towards lining and stitch down leaving enough space to pull trough elastic.

![](013web.jpg) Cut 80-85cm of elastic band and pull elastic through both tunnels using appropriate tool or safety pin.

![](014aweb.jpg) Tie up ends at desired lengt on the bottom or use cor lock stopper.

![](014bweb.jpg) Alternatively pull a single piece of elastic trough each side to create loops for ears.

![](20200330_174534web.jpg) Insert filter in pocket and replace after use. According to some [research](https://masks4all.co/how-to-make-a-homemade-mask/) a paper
towel can be used as a filter.

![](20200330_174651web.jpg) DIY filter made from three layers of meltblown nonwoven fabric

** Download pattern here:** 

- [](face_mask_pattern.pdf)

### Commercial Protective Masks

The following is research about various commercially available
protective masks

- [3M report published Feb 2020 on Respiratory Protection for Airborne Exposures to Biohazards](https://multimedia.3m.com/mws/media/409903O/respiratory-protection-against-biohazards.pdf) (VERY GOOD)
- [N95 Masks vs. Surgical Masks](https://smartairfilters.com/en/blog/n95-mask-surgical-prevent-transmission-coronavirus/)
- [F2 high flow filters](https://rzmask.com/products/f2-filter-high-flo)
- [Sundström SR200 Full Mask](https://en.resinance.de/product/sundstroem-sr-200/)
- [Sundström SR200 technical details](https://www.srsafety.com/us/sr-200-full-face-mask-pc-h01-1221.html)
- [Details on P100 filters](https://pksafety.com/blog/what-does-p100-mean)
- [3M 60926 Multi-Gas P100 Cartridge](https://pksafety.com/3m-60926-multi-gas-p100-cartridge-pair/)
- [CDC NIOSH-Approved P100 Particulate Filtering Facepiece Respirators](https://www.cdc.gov/niosh/npptl/topics/respirators/disp_part/p100list1.html)
- [3M™ Particulate Filters 5935, P3](https://www.3m.com.au/3M/en_AU/company-au/all-3m-products/~/3M-Particulate-Filters-5935-P3/?N=5002385+8711017+3294471746&rt=rud)
- [List of filters and cartridges](https://www.respiratormaskprotection.com/Respirator-Cartridge-Filter-Reference-Chart.php)
- [Dust Mask Ratings: FFP1 vs FFP2 vs FFP3 - The Ultimate Guide](https://www.xamax.co.uk/blog/how-to-pick-a-dust-mask-rating-p1-vs-p2-vs-p3.html#p3-ffp3)
- [Research on improvised facemasks at Cambridge Uni](https://smartairfilters.com/en/blog/best-materials-make-diy-face-mask-virus/)
- [Testing the Efficacy of Homemade Masks Would They Protect in an Influenza Pandemic](https://www.researchgate.net/publication/258525804_Testing_the_Efficacy_of_Homemade_Masks_Would_They_Protect_in_an_Influenza_Pandemic)

**Purchasing Protective Masks Online**

- [3M 6000 series half mask](https://www.amazon.de/dp/B00EJIMZT8/ref=cm_sw_r_tw_dp_U_x_sxTxEbMQGGT9D) - Amazon DE
