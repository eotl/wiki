---
title: "Home Decontamination"
draft: false
leafs: ["decontamination", "sterilization"]
date: "2020-08-26 12:52:22"
date_created: "2020-08-26 12:52:22"
type: "wiki"
branches: "crisis-infrastructure"
---

There are many different ways to decontaminate at home. This a
non-exhaustive place to document things found on the net.

### Ultraviolet (UV) Germicidal Irradiation

Read up on the basics on Wikipedia:

- [Ultraviolet germicidal irradiation](https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation)
- [Ultraviolet](https://en.wikipedia.org/wiki/Ultraviolet#Subtypes)

Claims:

253nm wavelength is optiminal. Lamps that are 11-13 watt will cover a
distance of 15-20 sq for a duration of about 45-60 minutes. There are
low pressure bulbs and medium pressure bulbs\[3\]. Many UVC lamps form
an ozone which will smell, this is in the range of 185nm \[2\].

Places to purchase online:

- [13W UVC Lamp Amazon UK](https://www.amazon.co.uk/dp/B085GF2XJT/ref=cm_sw_r_tw_dp_U_x_jjTAEbEKRX8X4)
- [Philips TUV Compact lamp PL-S 7 Watt UV-C](https://www.amazon.de/dp/B006D7LNVE/ref=cm_sw_r_tw_dp_U_x_iOQAEbG0SXM51)

UV Light for Water Treatment:

- https://www.light-sources.com/solutions/germicidal-uvc-lamps/uv-light-applications/uv-water-purification/uv-light-disinfection-water-treatment/
- https://puretecwater.com/downloads/basics-of-uv.pdf
- https://www.uvsterilizerreview.com/2009/03/uvc-watts-microwatts.html
- http://www.americanaquariumproducts.com/AquariumUVSterilization.html
- https://www.uvsterilizerreview.com/2013/10/actual-uv-c-emission-from-uv-bulb.html
- http://www.americanaquariumproducts.com/TMCUVSterilizer.html#vecton4
- https://ultraviolet.com/what-is-germicidal-ultraviolet/
- [Conversion chart](http://www.safelivingtechnologies.ca/PDF's/Conversion%20Chart%20%C2%B5W-1.pdf)
- https://www.americanairandwater.com/water/wastewater-uv.htm
- [Technical Guide to Using UV Sanitation on Swimming Pools](https://www.poolspanews.com/facilities/maintenance/technical-guide-to-using-uv-sanitation-on-swimming-pools_o)
- [Research on far UVC currently being deployed as a product in healthcare](https://newatlas.com/far-uvc-airborne-viruses/53349/)
- [UV light can kill airborne flu virus study finds](https://www.upi.com/Science_News/2018/02/09/UV-light-can-kill-airborne-flu-virus-study-finds/3081518201355/)
- [UVC Safety at University of California](https://www.ehs.uci.edu/programs/radiation/UV%20Lamp%20Safety%20Factsheet.pdf)
- [Good overview and safety](https://www.klaran.com/is-uvc-safe)
- [European Commission on the effects of ultraviolet radiation](https://ec.europa.eu/health/ph_risk/committees/04_sccp/docs/sccp_o_031b.pdf)
- [Info on sunglasses for PPE](https://www.allaboutvision.com/sunglasses/spf.htm)
- [Example UV protective glasses](https://www.riskreactor.com/black-lights/uv-safety-glasses/uvglass-00-uva-uvb-and-uvc-protective-orange-safety-glasses/)
- [Brochure for commercially available UV Decontamination box](https://www.grainger.com/ec/pdf/Air-Science-UV-Box-Brochure.pdf)

This box contains 3 x 254 nanometer 60 watt bulbs for a combined 180watt
and a timer that goes up to 60minutes

For protection, use UV 400 glasses

- https://www.reference.com/world-view/uv-400-8eae3f9a5dc049bd

There is evidence that the ozone produced by UVC light can have harmful
effects on humans


- https://www.greenfacts.org/en/ozone-o3/l-2/2-health-effects.htm
- https://www.allergyclean.com/problems-with-ozone-generators-and-ionizers-that-produce-ozone/
- [3M P100 filters](https://www.3m.com/3M/en_US/company-us/all-3m-products/~/3M-Particulate-Filter-2091-07000-AAD-P100-100-EA-Case/)
- [Effects of Ultraviolet Germicidal Irradiation (UVGI) on N95 Respirator Filtration Performance and Structural Integrity](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4699414/)
- [Reusing Face Masks To Prevent Airborne Illnesses](https://www.youtube.com/watch?v=PGHMU4iegLE) (YouTube)

### Wands

- [The Portable Sanitizing UV Wand](https://www.hammacher.com/product/virus-destroying-uv-wand-) - $146 USD
