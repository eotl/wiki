---
title: "Sustainable Villages"
draft: false
leafs: ["sustainable", "villages"]
date: "2022-05-21 07:03:23"
date_created: "2022-05-21 07:03:23"
type: "wiki"
branches: "housing"
---

Here and there small newly village like groups sprout up. Here is a page
to document such experiments in housing.

## Asia

- [Kamikatsu in Japan is transforming into a zero-waste town](https://www.timeout.com/tokyo/travel/kamikatsu-in-japan-is-transforming-into-a-zero-waste-town), TimeOut, 28 July 2022
