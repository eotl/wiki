---
title: "Living Liquids"
draft: false
leafs: ["living", "liquid"]
date: "2022-05-21 06:54:10"
date_created: "2022-05-21 06:54:10"
type: "wiki"
branches: "food-hacks"
---

A place to collect methods and required equipment for making:

- Kombucha
- Kefir
