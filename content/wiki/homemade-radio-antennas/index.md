---
title: "Homemade Radio / WiFi Antennas"
draft: false
leafs: ["homemade", "radio", "wifi", "antenna"]
date: "2020-03-30 18:25:57"
date_created: "2020-03-30 18:25:57"
type: "wiki"
branches: "information-technology"
---

The following is a collection of additional images and configurations of
[https://martybugs.net/wireless/collinear.cgi](this tutorial) of an
easy-to-make collinear 360 degrees omni-directional, vertically
polarised, antenna for 802.11b/g wireless networking.
