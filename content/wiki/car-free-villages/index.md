---
title: "Car Free Villages"
draft: false
leafs: ["car", "free", "village"]
date: "2022-06-20 09:40:57"
date_created: "2022-06-20 09:40:57"
type: "wiki"
branches: "housing"
---

The idea is sprouting up of making cities or villages more car free. The
following are some extreme examples:

### Las Catalinas, Costa Rica

![](/las-catalinas-plaza.jpg){width="400"}

[Photo NY Times, 2019](https://www.nytimes.com/2019/06/18/realestate/costa-rica-carless-community-las-catalinas.html)

Las Catalinas is a beach town developed in 2006 along the shores of the
Pacific Ocean in the Guanacaste Province of northwest Costa Rica. The
objective was to create a compact, car-free, and fully walkable town,
based on the principles of New Urbanism.
[\~Wikipedia](https://en.wikipedia.org/wiki/Las_Catalinas%2C_Costa_Rica)

Discovered via new urbanist researcher [Samuel Hughes](https://twitter.com/SCP_Hughes/status/1538525957375823872)

- [lascatalinascr.com](https://www.lascatalinascr.com)
