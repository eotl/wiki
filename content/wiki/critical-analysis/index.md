---
title: "Critical Analysis"
draft: false
leafs: ["analysis"]
date: "2022-03-01 16:59:31"
date_created: "2022-03-01 16:59:31"
type: "wiki"
branches: "network"
---


- [Carbolytics](https://carbolytics.org) is a project at the
    intersection of art and research that aims to raise awareness and
    call for action on the environmental impact of pervasive
    surveillance within the advertising technology ecosystem (AdTech)

### Cryptocurrency

While considering exciting and a necessary future for some people, for
others cryptocurrencies are a total disaster for the environment and
should be shunned and banned by governments. Here is a place to collect
links for, against, and analyzing cryptocurrencies impact on the
environment.

**Energy trackers and analysis**

- [Ethereum Emissions](https://kylemcdonald.github.io/ethereum-emissions/) is a
    dashboard tracking Ethereum energy use
- [Netpositive Money](https://netpositive.money) an initiative by
    bitcoiners who want to contribute to climate change solutions

**Mining and energy used**

- [Costa Rica hydro plant gets new lease on life from crypto mining - Reuters](https://www.reuters.com/technology/costa-rica-hydro-plant-gets-new-lease-life-crypto-mining-2022-01-11/)
