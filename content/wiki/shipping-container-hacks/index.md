---
title: "Shipping Container Hacks"
draft: false
date: "2021-09-21 20:20:46"
date_created: "2021-09-21 20:20:46"
type: "wiki"
leafs: ["shipping-containers", "hacks"]
branches: "housing"
---

- [Claudie Dubreuli's shipping container home](https://www.livinginacontainer.com/claudie-dubreuils-shipping-container-home-canada/) is a fancy design setup in Canada
- [The Farmery](https://inhabitat.com/the-farmery-a-pop-up-urban-farm-made-from-recycled-shipping-containers-for-raleigh-nc/) is a startup in Raleigh, N.C.
