---
title: "Mobile Greenhouses"
draft: false
leafs: ["mobile", "greenhouses"]
date: "2020-11-04 11:52:05"
date_created: "2020-11-04 11:52:05"
type: "wiki"
branches: "essentials"
---

- [Box Truck Converted into MOBILE GREENHOUSE](https://www.youtube.com/watch?v=h-g74F-U9yU) (video)
- [Grow-Trailers & Mobile-Farmers of America! Art Garden/Grow-Mobile](https://www.youtube.com/watch?v=VqUfruOp_9k) (video)
- [Elaborate hydroponic growing system in a shipping container](https://www.youtube.com/watch?v=N0_1ykb2xH0) (video)
- [Inside a Shipping Container Vertical Hydroponic Farm](https://www.youtube.com/watch?v=XhYcDKfgi7M) (video)

### Projects

- [Compass Green Project](http://compassgreenproject.org)
