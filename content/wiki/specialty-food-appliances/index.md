---
title: "Specialty Food Appliances"
draft: false
date: "2022-05-17 04:13:37"
date_created: "2022-05-17 04:13:37"
type: "wiki"
branches: "food-hacks"
leafs: ["food", "appliances"]
---

Many delicious foods that we enjoy eating that are purchased at
supermarkets are the output of numerous machines and appliances usually
in factories. Here is a page to collects specialty food appliances.
These can be purchased or store bought, but we are especially fond of
DIY, maker, tinker solutions due to our focus on self-reliability.

## Molds & Presses


### Tofu & Tempeh

During the last step of [making tofu](making-tofu) you need a mold with
drainage holes to pour the liquid tofu into. This simple mold was made
from a simple wood box with dividers by drilling holes in the bottom.

![](/diy-tofu-draining-box.jpeg){width="200"}

### Tortilla Presses

Tba...

## Mills, Presses, Grinders

To process grains, nuts, seeds, and other food items requires different
types of machines.

### Grains

- [Komo mills](https://komo.bio) is a family business that makes mills out of wood and metal. It is suggested to pick one from here that can do both hard and pseudo-hard grains as well as legumes.
- [GrainMaker - Grinding Mills](https://grainmaker.com/grinding-mills/) very sturdy all metal, two models \$675 - \$1200 also has a \"bike\" attachment
- [Getreidemühlen](https://www.getreidemuehlen.de) - selection of numerous German made wood hand mills
- [Country Living Grain Mill](https://store.countrylivinggrainmills.com/grain-mill/) - sturdy and simple looking \$529.00
- [Hawos](https://hawos.de) - large selection of electric and some hand powered mills 300 - 1800 EUR (DE)
- [KornKraft](https://www.kornkraft.de/component/virtuemart/muehlen-und-zubehoer?Itemid=0) - nice selection of hand powered only small mills 99 - 250 EUR (DE)

### Presses

- [GrainMaker - Sorghum or Sugarcane press](https://grainmaker.com/products/grainmaker-sorghum-press/) - very sturdy looking \$3200
- [Sana Supreme EUJ-727](https://www.gruene-kueche.de/shop/sana-supreme-euj-727/) - high-quality consumer juicer, engine does not develop any heat and can be run non-stop. One draw back is cleaning, it is made of plastic, no dishwasher. 699.00 €
- [Angel Juicer](https://www.gruene-kueche.de/angel-juicer-im-vergleich) - The \"ultimate\" machine is the Angel Juicer and is made of metal, very robust, so you can also clean the parts in the dish washer. Priced from 1319.00 to 1729.00 EUR
- [Kuvings Master Chef CS700](https://kuvings.de/collections/professional/products/cs700)  Professional juice press which can produce commercial scale amounts of juice €1.399,00

### Grinders

- Simple metal meat grinders, which on average cost around €20,00, can be used for all sorts of things [like peanut butter, soy milk making](https://twitter.com/hundredrabbits/status/1320874047845011456#m).

![](/wiki/meat-grinder-simple.jpeg){width="200"}


## Drinks

Producing drinks like sodas or alcohols require specialty machines.

### Sparkling Water Machines

A great way to avoid plastic bottles of beverages, as well as
environmental footprint of transporting heavy liquid filled glass
bottles in trucks long distances is to make your own sparkling water and
sodas. Home production of sparkling water just requires refilling a CO2
canister periodically and in recent years there have been companies
making affordable soda water machines for the house.

- [Aarke](https://www.aarke.com) prices start at €199,00
- [SodaStream](https://sodastream.com) prices start at €120,00

There are larger commercial offerings in this direction as well that you
attach to your faucet or plumbing but are usually more expensive.

- [Wasserkontor](https://shop.wasserkontor.de) a shop in Berlin

### Sprouting Jars

Producing sprouts from numerous seeds is a great way to produce edible
nutritious micro greens. Due to the size you can do this at small scale
in most kitchen apartments.

Sprouting jars can be made relatively easy with some simple mesh and
repurposed glass jars. More elaborate options are available for purchase
that allow for stacking and draining.
