---
title: "Solar Power"
draft: false
leafs: ["solar-power"]
date: "2020-11-02 10:36:17"
date_created: "2020-11-02 10:36:17"
type: "wiki"
branches: "energy-production"
permalink: "/solar-power"
---

A rough dump of various links and aspects of doing a solar power
generation.

- [Weather.com](https://weather.com/weather/radar/interactive/l/5ca23443513a0fdc1d37ae2ffaf5586162c6fe592a66acc9320a0d0536be1bb9)
    real time radar imaging of cloud cover in Berlin

Real world test between mono and polycrystaline panels

- https://youtube.com/watch?v=dZ_jrSAk-DU poly wins in this test

### Panels + Controllers

This looks like the solar panel mentioned in the review: https://www.amazon.de/dp/B082F3GTLG/ref=cm_sw_r_tw_dp_x_l2yeFbHDCZGFV

MPPT vs PWM: Fast comparison for off-grid solar:

- http://invidio.us/watch?v=PB6zojol9o0

Really good buyers guide to solar controllers

- https://www.invidio.us/watch?v=kF_cVEYxj3E

Review of flexible panels shows they do not work as well as the glass
mounted panels. This is because the heat distribution is uneven and the
cells can overheat and become damaged. The reviewer says that this can
mean they die within 5 years of use

- https://invidio.us/watch?v=onMZ_dRSFUs

Note that one commenter did cast doubt on some of the overheating issues
saying it happened due to circuitry:

```
Lance Alpuerto 1 year ago I agree that the inability to dissipate
heat would cause failure of the panels or at least reduce its
performance. Ambient temperature does account for a large portion of
heat, but what is being show at 0:46 is from circulating current due to
individual receiving different amounts of radiation, thus different
currents. It has more to do with internal interactions rather than
environmental temperatures. Also, this is an issue flat panels share, so
his statements aren\'t technically true. It just happens more often with
flexible panels because how they are being used.
```

### Batteries

This is a company in the US that makes large battery arrays with used
batteries

- [Big Battery](https://bigbattery.com/about/)

### Using solar in Europe

- [What can you run on a 100W Solar Panel?](https://youtube.com/watch?v=1nrAE5Q3lVQ)
- [Solar map](https://solargis.info/imaps/#loc=52.553931,13.386411&c=51.142434,8.266782&z=6)

Northern mainland Europe gets an average of 3.3 hours of sunshine per
day. When taking in to account 30% loss due to inefficiencies in the
solar system this means you could generate 231Wh per day on average for
each 100 Watt panel.

This means that if you wanted 2 days of backup power for moderate usage
of electronics such as laptops and phones you would need a 90 Amp hour
lead acid battery.

- [calculator for angling the panels](http://www.solarelectricityhandbook.com/solar-angle-calculator.html)


### DIY foldable panels

- https://youtube.com/watch?v=sGe90rG4aQ4

### Outdoor Housing

- https://uk.pi-supply.com/products/die-cast-outdoor-weatherproof-enclosure
- https://foter.com/outdoor-storage-box-wood

### German Solar Manufacturers

- https://solarfeeds.com/top-solar-manufacturers-in-germany/
- https://www.axsun.de/en/solar-panels/premium-ensemble - this company offers 15-25 year warranties
- https://www.axsun.de/en/references/examples-custom-solar-modules - their custom installs look really nice

### Notes

AGM batteries can only be drained down to 12.3V which is 50%, 10.5v is
100%:

- https://youtu.be/8F6AYavY870?t=362

If you take a lead acid battery cycles by how much you drain as a
percentage

- 20% Full - 250 cycles
- 50% Full - 500 Cycles
- 80% Full - 1,200 Cycles

Powering an Apple computer from 12v AC

- https://www.bigmessowires.com/2020/04/16/building-a-12v-dc-magsafe-charger/

Understanding C ratings on Lead Acid Batteries

- http://solarhomestead.com/battery-amp-hour-ratings/
- https://chargetek.com/basic-information.html
- http://www.leonics.com/support/article2_14j/articles2_14j_en.php

### Understanding the right voltage of a PV and Battery

- https://www.victronenergy.com/blog/2020/02/20/pv-panel-output-voltage-shadow-effect/

```
The Victron MPPT is a buck DC to DC converter. It reduces the
higher PV side voltage to the lower Battery side voltage. It can't boost
the (too low) voltage from a PV panel in order to begin charging a
battery. Working at up to 98% efficiency the MPPT can accept any PV side
voltage up to its maximum PV input voltage limit. This varies with the
Victron models between 75V and 250V and is clearly printed on the unit
itself, and all associated documentation.

On the battery side, it is the battery which sets the system voltage.
The MPPT takes the panel voltage and converts it to a charging voltage
which is higher than battery voltage in order to get current to flow
into the battery, the voltage is reduced, the current goes up, and the
power remains the same. But the battery chemistry will be dragging that
MPPT voltage down at the DC bus level, and that electrical work is going
into the battery chemistry to charge it.

Once the battery is full, and reaches a target voltage, the MPPT will
adjust its voltage conversion to maintain a pre-set float voltage which
is still higher than the battery voltage at rest -- but not high enough
to provide a significant current flow ...which avoids the battery
becoming overcharged.Bulk, Absorption, Float... Solar charger output
voltage depends on where the connected battery is in its charging cycle
(bulk, absorption, float) -- the voltage of each stage being pre-set by
battery charging algorithm employed by the MPPT. (The target voltage for
each step can also be user-defined.)

In the case of a nearly empty lead battery at 11.5V the MPPT begins work
by 'Bulk' charging with as much power as it can get from the solar
panel(s) (unless a lower current-limit has been set) until it reaches
the absorption voltage of 14.4V.

The MPPT will only begin charging when there is sufficient solar
radiation to cause the PV panel voltage to rise 5V above the Battery
voltage. After that condition has been met it will continue charging as
long as the PV voltage remains at least 1V higher than the Battery
voltage (or until the battery is full).

In the example above: The MPPT will begin charging when the panels
provide around 16.5V ...and will need a minimum of 12.5 V rising to
15.4V to continue charging.

When planning your installation you may find it helpful to use this
ready made solar charging calculator to choose suitably sized equipment.
```

### Common Mistakes

- https://solarpanelsvenue.com/mppt-and-pwm-charge-controllers-in-off-grid-solar-power-systems/

When connecting the inverter to the charge controller 'DC load'
terminal, check in the charge controller data sheet whether this
terminal is powerful enough to provide the input current to the
inverter. Otherwise, connect the higher power inverter directly to the
battery bank. In such a case, you will render the charge controller's
function that prevents the battery from overdischarging useless.

### State of Charge for Lead Acid

- https://modernsurvivalblog.com/alternative-energy/battery-state-of-charge-chart/
- https://www.energymatters.com.au/components/battery-voltage-discharge/

### Miscellaneous

- [Solar power analyser](https://www.amazon.de/dp/B07ZQJZ4WL/ref=cm_sw_r_tw_dp_x_-ZyeFb9RZTCZ4)
