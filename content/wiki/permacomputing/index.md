---
title: "Permacomputing"
draft: false
leafs: ["permacomputing", "computers", "e-waste"]
date: "2022-03-17 11:32:45"
date_created: "2022-03-17 11:32:45"
type: "wiki"
branches: "information-technology"
---

Borrowing from the philosophy of [permaculture](permaculture) farming,
there is the idea of [permacomputing](https://wiki.xxiivv.com/site/permacomputing.html) which
is based on frugality, salvage, and considering collapse scenarios.

### Software

Radical open-source OS and software aiming in an inverse direction from
popular computing (3D, GUIs, fancy graphics card, heavy power use, etc).
Essentials which one can use in ultra-low-impact or [off the
grid](https://100r.co/site/off_the_grid.html) scenarios.

- [Collapse OS](https://collapseos.org) is creating \"Bootstrap post-collapse technology\" so basically a computer for after modern society collapses.
- [uxn](https://100r.co/site/uxn.html) ecosystem is a personal computing playground

### Hardware

Radical new open-source hardware devices meant to be tinkered, repaired,
modified or entirely fabricated in a decentralized fashion.

- [MTN Reform](https://mntre.com/media/reform_md/2020-05-08-the-much-more-personal-computer.html) a fully open source laptop created for intensive tinkering + upgradablity in mind.

### Components

Fresh starts and approaches to making basic components for computing in
open and sustainable ways.

- [Sam Zeloof \@szeloof](https://mobile.twitter.com/szeloof) of Carnegie Melon is making homebrew computer chips

### DIY Devices

Devices which allow for a full computing experience but can be
fabricated with minimal efforts, parts, etc.

- [Mini Computer Easel](Mini Computer Easel)
- [Mini Server Raspberry Pi 4](https://n-o-d-e.net/node_mini_server3.html) by N-O-D-E
- [Zero Terminal](https://n-o-d-e.net/zeroterminal3.html) by N-O-D-E

### Hacks

A nifty hack posted by [Chris Beckstrom](https://chrisbeckstrom.com) is
how to get [rca video output from a \#raspberrypi
zero](https://planet.chrisbeckstrom.com/notice/AGqnyT0HMzJZIWVkLg). In
Chris\' words \"do a tiny bit of soldering and there ya go. Really nice
way to avoid a million adapters if your destination is analog!\"

![](/raspberrypi-zero-rca-video.jpeg){width="400"}
