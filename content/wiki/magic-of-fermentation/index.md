---
title: "Magic Of Fermentation"
draft: false
leafs: ["magic", "fermentation"]
date: "2022-02-07 20:00:56"
date_created: "2022-02-07 20:00:56"
type: "wiki"
branches: "food-hacks"
---

The wonderful magic of fermentation is primarily about, but not limited
to, the following food preservation techniques:

- Tofu
- Tempeh
- Kombucha
- Kefir
- Mushrooms
- Kimchi
- Sauerkraut

### Starters & Courses

- [MyVeganFam](https://www.myveganfam.com) - artisan made Tempeh and
    cultures, Amsterdam, NL
- [Edible Alchemy](https://ediblealchemy.co/store/) - offers numerous
    cultures and starters as well as courses, Berlin, DE
- [Homemade Tempeh Starter](https://www.youtube.com/watch?v=qIxrdcEPAOg) YouTube video
- [Soy-Free Tempeh](https://www.youtube.com/watch?v=SKmwcLfWL-k) YouTube Video
- [KEEPING IT CLEAN: HOW TO DESIGN AND BUILD A LAMINAR FLOW HOOD](https://learn.freshcap.com/growing/keeping-it-clean-how-to-design-and-build-a-laminar-flow-hood/)

### Production

Some outstandingly nice designs and productions by a Barcelona based
open source food / design duo called [Domingo
Club](https://domingoclub.com).

- [Biolab Incubator v0.2](https://maudbausier.com/biolab-kitchen-incubator-v0.2.html) an open source incubator oven
- [Mushroom Incubator necklace](https://github.com/domingoclub/incubator-necklace)
- [3D printed Tempeh moulds](https://domingoclub.com/3D-printed-moulds-v-02.html)
