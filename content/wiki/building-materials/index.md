---
title: "Building Materials"
draft: false
leafs: ["building", "material"]
date: "2022-02-07 19:49:41"
date_created: "2022-02-07 19:49:41"
type: "wiki"
branches: "housing"
---

- [How to make natural plaster](https://www.onegreenplanet.org/lifestyle/how-to-make-natural-plaster/)
