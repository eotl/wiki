---
title: "Cargo Bicycles"
draft: false
leafs: ["cargo", "bicycle"]
date: "2022-05-21 07:02:47"
date_created: "2022-05-21 07:02:47"
type: "wiki"
branches: "transportation"
---

In recent years there has been an explosion in cargo bicycles available
on the market.

- [Urban Arrow](https://urbanarrow.com)
- [Bakfiets](https://www.bakfiets.com)
- [Radkutsche](https://www.radkutsche.de)
- [Omnium Cargo Bike](https://omniumcargo.dk)
- [Bullitt](https://www.splendidcycles.com/products/bullitt-cargo-bikes/)
- [Christiania Bikes](https://www.christianiabikes.com)
- [Black Iron Horse](https://blackironhorse.com)
