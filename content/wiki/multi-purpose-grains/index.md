---
title: "Multi Purpose Grains"
draft: false
leafs: ["multi", "purpose", "grain"]
date: "2020-03-06 16:51:44"
date_created: "2020-03-06 16:51:44"
type: "wiki"
branches: "food-hacks"
---

The following are a list of grains that can be processed to create
multiple food products while producing zero-waste of left over
byproducts.

### Soy Beans

- Homemade tofu
- Soy milk
- Falafel balls

### Oats

- Oat milk
- Incredible crackers
