---
title: "Reusable Systems"
draft: false
leafs: ["reusable", "system", "food", "storage"]
date: "2021-10-27 11:54:02"
date_created: "2021-10-27 11:54:02"
type: "wiki"
branches: "food-storage"
---

Across the world there have always been reusable systems when it comes
to food transportation, storage and consumption. It\'s only within the
last half a century with industrial plastic production that disposable
throw away packaging came into being the dominant method.

Given 2000s hyper capitalist mode of production / coordination, there
are new companies emerging exploring "systems" of reusable containers.

- https://goboxpdx.com/how-go-box-works-customers/
