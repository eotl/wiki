---
title: "Mini Computer Easel"
draft: false
leafs: ["computers"]
date: "2022-03-17 11:33:43"
date_created: "2022-03-17 11:33:43"
type: "wiki"
branches: "network"
---

A neat form factor idea for an easel style SBC computer with very few
custom parts, casings, etc. adheres to [permacomputing](permacomputing)
and perhaps also [scavenger-tech](scavenger-tech) approaches.

![](/mini-computer-easel-1.jpeg){width="400"}
![](/mini-computer-easel-2.jpeg){width="400"}

[See rough tutorial thread by @tendigits](https://merveilles.town/@tendigits/107967889537981911)

**Parts Required**

- SBC Computer
- [HDMI 10\" LCD Screen Kit (1024x768)](https://shop.pimoroni.com/products/hdmi-10-lcd-screen-kit-1024x768?variant=21253917704275) by Pimoroni, 100 EUR
- Backing plate (acrylic, plastic, wood, metal)?
- Standing peg screws

**Equipment Required**

- 3D printer
- Screwdriver
- Lazer cutter or jigsaw

**Ideas & Notes**

This design\'s form factor above shared by
[@tendigits](https://tendigits.space) is super neat. Given nature of
SBC\'s one can of course refashion this to accommodate numerous
different sized / models of boards. Some random ideas and questions to
explore:

- Holster backing for large rechargeable battery pack like [Powerbank MP20](https://sonnenrepublik.de/shop-de/powerbank-mp20) to be attached to the easel for an easily movable setup.
- Simple enclosure box to protect the SBC and circuit board (while less cool, might be good idea)
- To create an open-hardware board which one could removing monitors from old broken laptops and repurposing them for a design like this.
- The boards to attach an MSATA solid state drive [Mini Server Raspberry Pi 4](https://n-o-d-e.net/node_mini_server3.html) by N-O-D-E 
