---
title: "Energy Saving Hacks"
date: "2020-09-01 12:19:25"
date_created: "2020-09-01 12:19:25"
type: "wiki"
leafs: ["energy", "hacks", "conservation"]
branches: "energy-production"
---

Organizing things around a house to share role of heating such as a heater and
stove is common in many cultures since hundreds of years.

![Image of Japanese home heating](japanese-home-heating.jpg)

When boiling water in a pot, the heat going off the edges of the pot is usually
lost into the air. Placing other items (colder water, frozen jars, etc) as a
heat-exchange layer helps that energy not be wasted.

![Image of household heat exchanger](honey-heat-exchanger.jpg)
