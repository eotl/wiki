---
title: "Sprouting & Shrubing"
draft: false
leafs: ["sprouting", "shrubin"]
date: "2022-05-21 06:52:34"
date_created: "2022-05-21 06:52:34"
type: "wiki"
branches: "essentials"
---

Growing things from sprouts or shrubs

### Sprouting

Tbw...

### Shrubing

A dead simple technique for growing plants from litte branches or
"shrubs" of other plants. A three step explainer as documented by
[Barnaby Walters](https://waterpigs.co.uk/notes/5CcPfy/) on his site.
Barnaby used a simple tape grid over a plastic tub of water, which was
is a nice low-tech solution to hold cuttings upright and loose.

![](//waterpigs-shrubing-step-1.jpeg){width="400"}

![](//waterpigs-shrubing-step-2.jpeg){width="400"}

![](//waterpigs-shrubing-step-3.jpeg){width="400"}
