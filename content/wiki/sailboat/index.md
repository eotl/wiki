---
title: "Sailboats"
draft: false
leafs: ["sailboats"]
date: "2022-06-06 09:31:58"
date_created: "2022-06-06 09:31:58"
type: "wiki"
branches: "transportation"
---

There appears to be a growing movement of sailors, boat builders trying
to create a new global movement of sustainable cargo shipping by
sailboat. A cornerstone connecting all these is that shipping cargo by
sail is emissions free. So cool.

#### Projects

- [Shipped by Sail](https://www.shippedbysail.org) - eco shipping broker and importer [\@shipperbysail](https://twitter.com/shippedbysail)
- [Fairtransport](https://fairtransport.eu) - operating and delivering Olive Oil and Coffee in Europe
- [EcoClipper](https://ecoclipper.org) - crowdfunding construction of clipper ship [\@EcoClipper](https://twitter.com/EcoClipper)
- [Sailcargo Inc.](https://www.sailcargo.org) - based in Costa Rica, building boats of wood
- [Sail Boat Project](https://sailboatproject.org) - workers co-op doing sailing education and small cargo delivery promoted online [\@SailBoatProject](https://twitter.com/SailBoatProject)
- [Brigantes](https://www.brigantes.eu) - Italian and Austrian [\@BrigantesShip](https://twitter.com/brigantesship)
- [Raybel Charters](https://raybelcharters.com) - sailing cargo up the Thames [\@RaybelCharters](https://twitter.com/RaybelCharters)
- [Blue Schooner Company](https://blueschoonercompany.com) - led by Guillaume Roche and Jeff Lebleu, joined by Julien Pouezevar.
- [Timbercoast](https://timbercoast.com) - cargo under sail operation based in Germany

#### Organizations

- [Sail Cargo Alliance](http://sailcargoalliance.org) - An alliance of organisations who share a passion for sail-shipped cargo

#### Articles

- [Low-Tech Magazine: How to design a sailing ship for the 21st century?](https://www.lowtechmagazine.com/2021/05/how-to-design-a-sailing-ship-for-the-21st-century.html)
- [Coffee Shipped by Sailboats In Efforts To Disrupt The Heavy Ships' Command Of The High Seas](https://gcaptain.com/coffee-shipped-by-sailboats-in-effofts-to-distrupt-the-heavy-ships-command-of-the-high-seas/)
- [Winds of change: the sailing ships cleaning up sea transport](https://www.theguardian.com/world/2019/oct/23/sailing-ships-cleaning-up-sea-transport-oceans)
