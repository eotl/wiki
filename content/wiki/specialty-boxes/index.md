---
title: "Specialty Boxes"
draft: false
leafs: ["boxes", "containers"]
date: "2020-08-12 16:40:26"
date_created: "2020-08-12 16:40:26"
type: "wiki"
branches: "containers"
---

- [BAUHAUS - Tabletteinsatz S](https://www.bauhaus.info/holzkisten/tabletteinsatz-s/p/24651815) - open top wood box with divider (small) - 4.14 EUR
- [BAUHAUS - Tabletteinsatz L/M](https://www.bauhaus.info/holzkisten/tabletteinsatz-lm/p/24652775) - open top wood box with divider (large) - 6.48 EUR
