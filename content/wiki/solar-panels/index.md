---
title: "Solar Panels"
date: "2020-08-14 12:19:00"
date_created: "2020-08-14 12:19:00"
type: "wiki"
branches: "energy-production"
leafs: ["solar", "energy", "power", "technology"]
---


## Various Notes


There is no protection against any shading, hotspots etc. in all these 18V
modules (e.g. by diodes). The reason is that such modules are normally not
connected to others in series, which is the only reason for getting hotspots and
using bypass diodes. Diodes against partial shading would only make sense when
connected to each cell which is a very special + expensive design. I don’t know
series modules with such a design.
 
Our surf modules have a special glass fiber backplate design which you find only
in better and higher prices modules.
 
The DC converter is a simple China type which has been modified from us with our
ow analogous MPP control unit. The max. output power is about 40W which fits
well to the iForway Theoretically you could connect various 40 - 100Wp 17-19Vmpp
modules since the iForway will just draw 38W max.   
