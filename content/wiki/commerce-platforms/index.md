---
title: "Commerce Platforms"
draft: false
leafs: ["commerce", "platform"]
date: "2020-10-04 13:22:42"
date_created: "2020-10-04 13:22:42"
type: "wiki"
branches: "information-technology"
---


When running businesses like store fronts, needing POS (Point Of Sale)
systems are essential. The following are solutions known to be used by
zero-waste and sustainable businesses.

- [BioOffice](https://www.biooffice-kassensysteme.de)
