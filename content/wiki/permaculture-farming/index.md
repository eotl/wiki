---
title: "Permaculture Farming"
draft: false
leafs: ["permaculture", "farming"]
date: "2022-02-28 20:35:01"
date_created: "2022-02-28 20:35:01"
type: "wiki"
branches: "essentials"
---

This is catch all page to collect links, tutorials, and videos about
general permaculture farming techniques and practices.

### Regenerative farming

- [DIY Chicken Tractor for Less than $200](https://abundantpermaculture.com/chicken-tractor-diy/) (tutorial)
- [Regenerative Agriculture / Regenerative Farming](https://youtube.com/watch?v=VEZvF68sytc) (video)
- [Regenerative Renegades | Natural Grocers Presents](https://youtube.com/watch?v=BkOb9Q2hXYE) (video)

### Irrigation

**Olla Pots** are a concept [dating back to Roman
times](https://en.wikipedia.org/wiki/Olla) for how to achieve a simple
irrigation of crops.

- [How to make DIY Ollas: Low Tech Self-Watering Systems for Plants](https://lovelygreens.com/how-to-make-diy-ollas-low-tech-self-watering-systems-for-plants/) (tutorial)
- [Make Your Own  Ollas](https://suburbanfarmonline.com/2010/08/09/make-your-own-ollas/) (tutorial)
