---
title: "Solar Punk Manifesto"
draft: false
leafs: ["solarpunk", "manifesto"]
date: "2020-08-19 12:42:26"
date_created: "2020-08-19 12:42:26"
type: "wiki"
branches: "network"
---

The following is a draft of a Solar Punk Manifesto crowdsourced on the
blog [Permaculture
Solarpunk](https://alexardov-blog.tumblr.com/post/187082500092/solarpunk-manifesto).
In the spirit of decentralization, backups, and not using Google, we\'ve
made a copy of the draft here. As the author states: Many people have
written about Solarpunk during the last 10+ years. Mostly after 2014.
The genre is not yet clearly defined.

This Solarpunk Manifesto is a creative re-adaptation of ideas about
solarpunk written by many people. These ideas can be mainly found
[here](https://medium.com/solarpunks/solarpunk-a-reference-guide-8bcf18871965)
in *Solarpunk: a reference guide* and
[here](https://medium.com/solarpunks/solarpunk-a-reference-guide-8bcf18871965)
in *Solarpunk: Notes towards a Manifesto* by Adam Flynn.


### A Solarpunk Manifesto

```
Solarpunk is a movement in speculative fiction, art, fashion, and
activism that seeks to answer and embody the question *\"what does a
sustainable civilization look like, and how can we get there?\"*

The aesthetics of solarpunk merge the practical with the beautiful, the
well-designed with the green and lush, the bright and colorful with the
earthy and solid.

Solarpunk can be utopian, just optimistic, or concerned with the
struggles en route to a better world ,  but never dystopian. As our
world roils with calamity, we need solutions, not only warnings.
Solutions to thrive without fossil fuels, to equitably manage real
scarcity and share in abundance instead of supporting false scarcity and
false abundance, to be kinder to each other and to the planet we share.
Solarpunk is at once a vision of the future, a thoughtful provocation, a
way of living and a set of achievable proposals to get there.

1.  We are solarpunks because optimism has been taken away from us and
    we are trying to take it back
2.  We are solarpunks because the only other options are denial or
    despair
3.  At its core, Solarpunk is a vision of a future that embodies the
    best of what humanity can achieve: a post-scarcity, post-hierarchy,
    post-capitalistic world where humanity sees itself as part of nature
    and clean energy replaces fossil fuels
4.  Solarpunk is a movement as much as it is a genre: it's not just
    about the stories, it's also about how we can get there
5.  Solarpunk embraces a diversity of tactics: there's no single right
    way to do solarpunk. Instead, diverse communities from around the
    world adopt the name, ideas, or both, and build little nests of
    self-sustaining revolution
6.  Solarpunk provides a valuable new perspective, a paradigm and a
    vocabulary through which to describe one possible future. Instead of
    retrofuturism, the theme turns completely to the future. Not an
    alternative future, but a possible future
7.  Our futurism is not nihilistic like cyberpunk and it avoids
    steampunk's potentially quasi-reactionary tendencies: it is about
    ingenuity, generativity, independence, and community
8.  The "punk" in Solarpunk is about rebellion, counterculture,
    post-capitalism, decolonialism and enthusiasm. It is about going in
    a different direction than the mainstream, which is increasingly
    going in a scary direction
9.  Solarpunk emphasizes environmental sustainability and social justice
10. Solarpunk is about finding ways to make life more wonderful for us
    right now, and also for the generations that follow us -- i.e.,
    supporting human life at the species level, rather than individually
11. Our future must involve repurposing and creating new things from
    what we already have. Imagine "smart cities" being junked in favor
    of smart citizenry
12. Solarpunk recognizes the historical influence politics and science
    fiction have had on each other
13. Solarpunk recognizes science fiction as not just entertainment but
    as a form of activism
14. Solarpunk wants to counter the scenarios of a dying earth, an
    insuperable gap between rich and poor, and a society controlled by
    corporations. Not in hundreds of years, but within reach
15. Solarpunk is about youth maker culture, local solutions, local
    energy grids, ways of creating autonomous functioning systems. It is
    about loving the world
16. Solarpunk culture includes all cultures, religions, abilities,
    sexes, genders and sexual identities
17. Solarpunk is the idea of humanity achieving a social evolution that
    embraces not mere tolerance, but a more expansive compassion and
    acceptance
18. The visual aesthetics of Solarpunk are open and evolving. As it
    stands, it's a mash-up of the following:
    -   1800s age-of-sail/frontier living (but with more bicycles)
    -   Creative reuse of existing infrastructure (sometimes
        post-apocalyptic, sometimes present-weird)
    -   Appropriate technology
    -   Art Nouveau
    -   Hayao Miyazaki
    -   Jugaad-style innovation from the developing world
    -   High-tech backends with simple, elegant outputs
19. Solarpunk is set in a future built according to principles of new
    urbanism and environmental sustainability 
20. Solarpunk envisions a built environment creatively adapted for solar
    gain amongst other arrangements and technologies to promote self
    sufficiency and living within natural limits
21. In Solarpunk we've pulled back just in time to stop the slow
    destruction of our planet. We've learned to use science wisely, for
    the betterment of ourselves and our planet. We're no longer
    overlords. We're caretakers. We're gardeners
22. Solarpunk is a future with a human face and dirt behind its ears
23. Solarpunk:
    -   is diverse
    -   has room for spirituality and science to coexist
    -   is beautiful
    -   can happen. Now

The Solarpunk Community
```
