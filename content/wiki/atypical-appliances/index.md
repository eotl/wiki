---
title: "Atypical Appliances"
draft: false
leafs: ["atypical", "appliance"]
date: "2021-09-11 20:06:34"
date_created: "2021-09-11 20:06:34"
type: "wiki"
branches: "machines-appliances"
---

A place to document various approaches of re-imagining common household
and work appliances- can these modern marvels we consider essential and
often take for granted be done radically differently to be radically
more sustainable?

### Cleaning

- [Make: Off-Grid Laundry Machine](https://makezine.com/projects/off-grid-laundry-machine)
