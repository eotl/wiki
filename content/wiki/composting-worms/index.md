---
title: "Composting & Worms"
draft: false
leafs: ["composting", "worm"]
date: "2022-01-24 22:37:02"
date_created: "2022-01-24 22:37:02"
type: "wiki"
branches: "network"
---

Ready made worm boxes

- [Wurmkiste](https://wurmkiste.at) €79 - 179
- [Terrabox](https://www.terrabox.bio) €250
- [WurmUp](https://eu.wormup.ch/#!/WormUp-HOME-Wurmkomposter-aus-Keramik/p/101071244/category=0) €279
