---
title: "Farming Software"
draft: false
leafs: ["farming", "softwar"]
date: "2022-06-16 23:44:58"
date_created: "2022-06-16 23:44:58"
type: "wiki"
branches: "food-hacks"
---

- [Tend Smart Farm](https://www.tend.com/page/features.html)
- [Gro Intelligence](https://gro-intelligence.com)
