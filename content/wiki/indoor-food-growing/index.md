---
title: "Indoor Food Growing"
draft: false
date: "2022-06-16 23:54:46"
date_created: "2022-06-16 23:54:46"
type: "wiki"
leafs: ["indoor", "food", "growing"]
branches: "essentials"
---

- [Growing Mushrooms in Coffee Grounds at No Cost](https://www.tinyplantation.com/vegetables/mushrooms/growing-mushrooms-in-coffee-grounds)
- [Growing sprouts and micro-greens in coffee grounds](https://twitter.com/hundredrabbits/status/1324035156424884224)
- [Tip when growing store bought basil plants](https://merveilles.town/@neauoire/107063805300398648)
- [How to grow a walnut tree from a nut](https://wikifarmer.com/how-to-grow-walnut-tree-from-nut/)

#### Misc Equipment

 - [KEEPING IT CLEAN: HOW TO DESIGN AND BUILD A LAMINAR FLOW HOOD](https://learn.freshcap.com/growing/keeping-it-clean-how-to-design-and-build-a-laminar-flow-hood/)

#### Full Systems

- [OGarden](https://ogardengroup.com) - an all inclusive barrel style design with digital system

#### Suppliers

- [Pilzmännchen](https://www.pilzzuchtshop.eu) German mushroom growing supplier

#### Misc

- [The Morel Project](https://thedanishmorelproject.com/the-morel-project/)
