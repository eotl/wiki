---
title: 'Essentials'
description: ""
icon: "branches"
status: "active"
url: ""
related: []
---

Things which are essential to human life. Essential, as in, the physiological base layer of
[Mazslow's hierarchy of needs](https://en.wikipedia.org/wiki/Maslow's_hierarchy_of_needs).
